package com.team.interfaces;

import com.team.model.Context;

public interface ScrumState {

    default void startSprintBacklog(Context context) {
        throw new UnsupportedOperationException();
    }

    default void toSprintBacklog(Context context) {
        throw new UnsupportedOperationException();
    }

    default void toDo(Context context) {
        throw new UnsupportedOperationException();
    }

    default void toReview(Context context) {
        throw new UnsupportedOperationException();
    }

    default void backToSprintBacklog(Context context) {
        throw new UnsupportedOperationException();
    }

    default void toTesting(Context context) {
        throw new UnsupportedOperationException();
    }

    default void toBlock(Context context) {
        throw new UnsupportedOperationException();
    }

    default void finish(Context context) {
        throw new UnsupportedOperationException();
    }

    default void toProductBacklog(Context context) {
        throw new UnsupportedOperationException();
    }

    default void scrumComplete(Context context) {
        throw new UnsupportedOperationException();
    }
}
