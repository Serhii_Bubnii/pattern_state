package com.team.interfaces;

public interface Printable {
    void print();
}
