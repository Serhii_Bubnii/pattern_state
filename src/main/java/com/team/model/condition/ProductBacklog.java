package com.team.model.condition;

import com.team.model.Context;
import com.team.interfaces.ScrumState;

public class ProductBacklog implements ScrumState {

    @Override
    public void startSprintBacklog(Context context){
        context.setScrumState(new SprintBacklog());
    }

    @Override
    public String toString() {
        return "State ProductBacklog";
    }
}
