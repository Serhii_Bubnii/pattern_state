package com.team.model.condition;

import com.team.model.Context;
import com.team.interfaces.ScrumState;

public class SprintBacklog implements ScrumState {

    @Override
    public void toDo(Context context) {
        context.setScrumState(new InProgress());
    }

    @Override
    public String toString() {
        return "State SprintBacklog";
    }
}
