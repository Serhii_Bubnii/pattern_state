package com.team.model.condition;

import com.team.model.Context;
import com.team.interfaces.ScrumState;

public class InProgress implements ScrumState {

    @Override
    public void toReview(Context context) {
        context.setScrumState(new PeerReview());
    }

    @Override
    public String toString() {
        return "State InProgress";
    }
}
