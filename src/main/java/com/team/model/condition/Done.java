package com.team.model.condition;

import com.team.model.Context;
import com.team.interfaces.ScrumState;

public class Done implements ScrumState {

    public void scrumComplete(Context context){
        context.setScrumState(new ProductBacklog());
    }

    @Override
    public String toString() {
        return "State Done";
    }
}
