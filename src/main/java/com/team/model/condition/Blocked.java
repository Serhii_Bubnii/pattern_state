package com.team.model.condition;

import com.team.model.Context;
import com.team.interfaces.ScrumState;

public class Blocked implements ScrumState {
    @Override
    public void toProductBacklog(Context context){
        context.setScrumState(new ProductBacklog());
    }

    @Override
    public String toString() {
        return "State Blocked";
    }
}
