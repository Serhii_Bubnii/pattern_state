package com.team.model.condition;

import com.team.model.Context;
import com.team.interfaces.ScrumState;

public class PeerReview implements ScrumState {

    @Override
    public void backToSprintBacklog(Context context) {
        context.setScrumState(new SprintBacklog());
    }

    @Override
    public void toTesting(Context context) {
        context.setScrumState(new InTest());
    }

    @Override
    public String toString() {
        return "State PeerReview";
    }
}
