package com.team.model.condition;

import com.team.model.Context;
import com.team.interfaces.ScrumState;

public class InTest implements ScrumState {

    @Override
    public void toSprintBacklog(Context context) {
        context.setScrumState(new SprintBacklog());
    }

    @Override
    public void toBlock(Context context) {
        context.setScrumState(new Blocked());
    }

    @Override
    public void finish(Context context) {
        context.setScrumState(new Done());
    }

    @Override
    public String toString() {
        return "State InTest";
    }
}
