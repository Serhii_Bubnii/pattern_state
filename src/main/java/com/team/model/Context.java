package com.team.model;

import com.team.model.condition.*;
import com.team.interfaces.ScrumState;

public class Context {

    private ScrumState scrumState = new ProductBacklog();

    public ScrumState getScrumState() {
        return scrumState;
    }

    public void setScrumState(ScrumState scrumState) {
        this.scrumState = scrumState;
    }

    public void toSprintBacklog() {
        this.getScrumState().toSprintBacklog(this);
    }

    public void toDo() {
        this.getScrumState().toDo(this);
    }

    public void toReview() {
        this.getScrumState().toReview(this);
    }

    public void backToSprintBacklog() {
        this.getScrumState().backToSprintBacklog(this);
    }

    public void toTesting() {
        this.getScrumState().toTesting(this);
    }

    public void startSprintBacklog() {
        this.getScrumState().startSprintBacklog(this);
    }

    public void toBlock() {
        this.getScrumState().toBlock(this);
    }

    public void toProductBacklog() {
        this.getScrumState().toProductBacklog(this);
    }

    public void finish() {
        this.getScrumState().finish(this);
    }
}
