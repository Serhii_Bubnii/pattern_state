package com.team.view;

import com.team.interfaces.Printable;
import com.team.model.Context;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {

    private Context context = new Context();
    private MessagePrinter print = new MessagePrinter();
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Scanner input;

    public View() {
        input = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        menu.put("1", "\t1 - Start scrum state");
        menu.put("Q", "\tQ - exit");
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::showStartScrumState);
    }

    private void showStartScrumState() {
        printStateScrum();
        print.printMessage("STATUS: " + context.getScrumState());
        print.printMessage("Enter 1 to get in SprintBacklog state");
        do {
            try {
                int state = input.nextInt();
                showStartSprintBacklog(state);
            } catch (UnsupportedOperationException e) {
                System.out.println("Incorrect number");
            }
        } while (true);
    }

    private void showStartSprintBacklog(int state) {
        switch (state) {
            case 1:
                context.startSprintBacklog();
                print.printMessage("Enter 2 to get in InProgress state");
                break;
            case 2:
                context.toDo();
                print.printMessage("Enter 3 to get in PeerReview state");
                break;
            case 3:
                context.toReview();
                print.printMessage("Enter 4 to get in SprintBacklog state");
                print.printMessage("Enter 5 to get in InTest state");
                break;
            case 4:
                context.backToSprintBacklog();
                print.printMessage("Enter 2 to get in InProgress state");
                break;
            case 5:
                context.toTesting();
                print.printMessage("Enter 6 to get in SprintBacklog state");
                print.printMessage("Enter 7 to get in Blocked state");
                print.printMessage("Enter 9 to get in Done state");
                break;
            case 6:
                context.toSprintBacklog();
                print.printMessage("Enter 2 to get in InProgress state");
                break;
            case 7:
                context.toBlock();
                print.printMessage("Enter 8 to get in ProductBacklog state");
                break;
            case 8:
                context.toProductBacklog();
                print.printMessage("Enter 1 to get in SprintBacklog state");
                break;
            case 9:
                context.finish();
                print.printMessage("Scrum state done");
                break;
            case 0:
                print.printMessage("STATUS: " + context.getScrumState());
                break;
            case -1:
                System.exit(0);
        }
    }

    public void show() {
        String keyMenu;
//        do {
        print.printMessage("------------------------------------------------------");
        outputMenu();
        print.printMessage("Please, select menu point.");
        keyMenu = input.nextLine().toUpperCase();
        quitProgram(keyMenu);
        try {
            methodsMenu.get(keyMenu).print();
        } catch (Exception e) {
            e.printStackTrace();
        }
//        } while (true);
    }

    private void quitProgram(String keyMenu) {
        if (keyMenu.equals("Q")) {
            System.exit(0);
        }
    }

    private void outputMenu() {
        print.printMessage("MENU: Pattern State");
        for (String str : menu.values()) {
            print.printMessage(str);
        }
    }

    private void printStateScrum() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("                                         4                \n")
                .append(" START                 -----------------------------------\n")
                .append("  ||                   ||                               /\\\n")
                .append("  \\/            1      \\/        2              3       ||\n")
                .append("ProductBacklog --> SprintBacklog --> InProgress --> PeerReview\n")
                .append("   /\\                    /\\                           5 || \n")
                .append("   || 8                  || 6                           \\/ \n")
                .append("Blocked <------------------------------------------- InTest \n")
                .append("                              7                      9 || \n")
                .append("                                                       \\/ \n")
                .append("                                                      DONE\n");
        System.out.println(stringBuilder);
    }
}
