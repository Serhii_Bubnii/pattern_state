package com.team.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MessagePrinter {

    private static Logger logger = LogManager.getLogger(View.class);

    public void printMessage(Object object) {
        logger.info(object.toString());
    }

}
