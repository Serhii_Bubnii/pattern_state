package com.team;

import com.team.view.View;

public class App {

    public static void main(String[] args) {
        new View().show();
    }
}
